﻿import time
import vk_api
import sqlite3
import random
import sys
import requests
import wolframalpha

vk = vk_api.VkApi(token = '***')
vk.auth();

weatherid = "0e36d1b3005e1b1f08e592cfb60075c7"

wolfram = wolframalpha.Client('AAY5LG-78QAA3PWX4')

values = {'out': 0,'count': 100,'time_offset': 10}

db = sqlite3.connect('main.db')

"""db.execute('''CREATE TABLE users
                (id INTEGER NOT NULL PRIMARY KEY, recourse TEXT)''')
db.execute('''CREATE TABLE secrets
                (id INTEGER NOT NULL, name TEXT NOT NULL, pass TEXT NOT NULL, data TEXT NOT NULL)''')"""


def write_msg(user_id, s):
    rec = get_recourse(user_id)
    if rec:
        s = rec + ', ' + s
    vk.method('messages.send', {'user_id':user_id,'message':s})

def get_recourse(id):
    test = show2('SELECT * FROM users')
    for i in test:
            if(i[0] == id):
                return i[1]
    return None

def change_recourse(id, text):
    if (text[1:8] == 'change ') and (len(text) > 8) :
        rec = text[8:]
        
        flag = 1
        if(get_recourse(id)):
            params = (rec, id)
            request = '''UPDATE users 
                         SET recourse = ?
                         WHERE id = ?'''
            db.execute(request, params)
            db.commit()
            
        else :
            params = (id, rec)
            request = ''' INSERT into users
                          (id,recourse)
                          VALUES(?,?)'''
            db.execute(request, params)
            db.commit()

        
        print(show2('SELECT * FROM users'))
 
def del_recourse(id):
    abc = get_recourse(id)
    if(get_recourse(id)):
        request = '''DELETE FROM users
                    WHERE id=?'''
        params = (id,)
        db.execute(request, params)
        db.commit()

        print(show2('SELECT * FROM users'))
    
def show(sql, params):
    try:
        cur = db.execute(sql, params)
        test =  cur.fetchall()
    except :
        abc = 3;
    return test

#версия без параметров
def show2(sql):
    cur = db.execute(sql)
    test =  cur.fetchall()
    return test

def myrnd(text):
    try:
        if len(text) > 10:
            if text[8:11] == 'int':
                pstrs = text.split(' ')
                if len(pstrs) == 2: #если /random int
                  return random.randint(0, 9223372036854775807)
                if len(pstrs) == 3: #если /random int <число>
                    if int(pstrs[2]) < 0 :
                        return random.randint(int(pstrs[2]), 0)
                    return random.randint(0, int(pstrs[2]))
                if len(pstrs) == 4: #если /random int <число> <число>
                    if int(pstrs[2]) - int(pstrs[3]) < 0:
                        return random.randint(int(pstrs[2]), int(pstrs[3]))
                    return random.randint(int(pstrs[3]), int(pstrs[2]))
            if text[8:11] == 'abc':
                pstrs = text.split(' ')
            if len(pstrs) == 3: #если /random abc
                return random.choice(pstrs[2])
            return 'введена не последовательность символов - присутсвуют пробелы!'
        if len(text) == 7:
            return random.random()
        return 'ошибка вычислений'
    except:
        return 'что-то не так с введенными данными!'

def secret(text, id):
    try:
        if len(text) > 10:
            pstrs = text.split(' ');
            if text[8:11] == 'add':
                if len(pstrs) < 5:
                    return 'недостаточно данных';
                c = 0
                pr4 = 0
                for i in text:
                    pr4 = pr4 + 1
                    if i == ' ':
                        c = c + 1
                        if c == 4:
                            break
                aoc = addsecret(pstrs, id, pr4, text);
                return 'секрет \"' + pstrs[2] + '\" успешно ' + aoc + '!';
            if text[8:11] == 'all':
                return get_secret_list(id)
            if text[8:11] == 'del':
                don = del_secrets(id, pstrs[2], pstrs[3])
                return 'секрет ' + str(don)
            if text[8:11] == 'get':
                ans = getsecret(id, pstrs[2], pstrs[3]);
                return ans;
            return 'проверьте правильность введенных данных'
    except:
        return 'проверьте правильность введенных данных'

def getsecret(id, name, passwd):
    
    if check_secret2(id, name, passwd) == 0:
        params = (id, name, passwd)
        request = 'SELECT data FROM secrets WHERE id = ? AND name = ? AND pass = ?'
        test = show(request, params)
        return 'текст секрета:\n' + test[0][0]
    else:
        if check_secret(id, name) == 0:
            return 'неверный пароль'
        else:
            return 'секрет не найден'

def addsecret(pstrs, id, pr4, text):
    if check_secret(id, pstrs[2]):
        
        params = (id, pstrs[2], pstrs[3], text[pr4:len(text)])
        request = ''' INSERT into secrets (id, name, pass, data)
                      VALUES (?,?,?,?)'''
        db.execute(request, params);
        db.commit();
        return 'добавлен'
    
    params = (id, pstrs[2], pstrs[3], text[pr4:len(text)], id, pstrs[2])
    request = '''UPDATE secrets
                 SET id = ?, name = ?, pass = ?, data = ?
                 WHERE id = ? AND name = ?'''
    db.execute(request, params);
    db.commit();
    return 'изменен'

def check_secret(id, name):
    line = 'SELECT * FROM secrets WHERE id = ? AND name = ?'
    try:
        test = show(line, (id, name));
        if len(test) == 0:
            return 1;
        return 0;
    except:
        return 1;

def check_secret2(id, name, passwd):
    line = 'SELECT * FROM secrets WHERE id = ? AND name = ? AND pass = ?'
    try:
        test = show(line, (id, name, passwd));
        if len(test) == 0:
            return 1;
        return 0;
    except:
        return 1; 

def get_secret_list(id):
    line = 'SELECT name FROM secrets WHERE id = ?'
    try :
        test = show(line, (id,))
        line = ''
        for i in test:
            line = line + '\"' + i[0] + '\"\n'  
        if len(line) > 0:
            line = 'найдены следующие секреты:\n' + line
        else :
            line = 'не найдено ни одного секрета'
        return line 
    except:
        return 'не найдено ни одного секрета'

def del_secrets(id, name, passwd):
    try:
        if check_secret2(id, name, passwd):
            return 'секрета с такими параметрами не найдено'
        request = '''DELETE FROM secrets
                     WHERE id = ? AND name = ? AND pass = ?'''
        params = (id, name, passwd)
        db.execute(request, params);
        db.commit();
        return 'успешно удален'
    except:
        return 'не удален: произошла ошибка'

def get_wind_direction(deg):
    l = ['северный ','северо-восточный',' восточный','юго-восточный','южный ','юго-западный',' западный','северо-западный']
    for i in range(0,8):
        step = 45.
        min = i*step - 45/2.
        max = i*step + 45/2.
        if i == 0 and deg > 360-45/2.:
            deg = deg - 360
        if deg >= min and deg <= max:
            res = l[i]
            break
    return res

def get_wind_direction2(deg):
    l = ['С ','СВ',' В','ЮВ','Ю ','ЮЗ',' З','СЗ']
    for i in range(0,8):
        step = 45.
        min = i*step - 45/2.
        max = i*step + 45/2.
        if i == 0 and deg > 360-45/2.:
            deg = deg - 360
        if deg >= min and deg <= max:
            res = l[i]
            break
    return res

# Проверка наличия в базе информации о нужном населенном пункте
def get_city_id(s_city_name):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                     params={'q': s_city_name, 'type': 'like', 'units': 'metric', 'lang': 'ru', 'APPID': weatherid})
        data = res.json()
        if(data['message'] != 'like'):
            return 'город не найден', 0
        cities = ["{} ({})".format(d['name'], d['sys']['country'])
                  for d in data['list']]
        city_id = data['list'][0]['id']
        city_name = data['list'][0]['name'] + ',' + data['list'][0]['sys']['country']
    except :
        print("Exception (find):")
     
    return city_name, city_id

# Запрос текущей погоды
def request_current_weather(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                     params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': weatherid})
        data = res.json()
        return 'Актуальность прогноза: ' +  str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(data['dt']))) + '\nОблачность: ' + str(data['weather'][0]['description']) + '\nТемпература: ' + str(data['main']['temp']) + ' °С\nМин. температура: ' +  str(data['main']['temp_min']) + ' °С\nMакс. температура: ' + str(data['main']['temp_max']) + ' °С\nДавление: ' + str(data['main']['pressure']) + ' гПа\nВлажность: ' + str(data['main']['humidity']) + '%\nВетер: ' + get_wind_direction(data['wind']['deg']) + ', ' + str(data['wind']['speed']) + ' м/с\nВосход: ' + str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(data['sys']['sunrise']))) + '\nЗакат:    ' + str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(data['sys']['sunset'])))
    except :
        return 'возникли проблемы при взаимодействии с сервером погоды'    

# Прогноз
def request_forecast(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                           params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': weatherid})
        data = res.json()
        line = '\n'
        for i in data['list']:
            line = line + (i['dt_txt'])[:16] + '{0:+3.0f}'.format(i['main']['temp']) + '{0:2.0f}'.format(i['wind']['speed']) + " м/с "+ get_wind_direction2(i['wind']['deg']) + '   ' + i['weather'][0]['description'] + '\n'
        return line
    except :
        return 'возникли проблемы при взаимодействии с сервером погоды'  

def hndl_cmd(text):
    id = item[u'user_id']
    if text[1:7] == 'change':
        change_recourse(id, text)
        write_msg(item[u'user_id'],u'принята команда к изменению обращения!\nОбращение успешно изменено на \"'+ get_recourse(item[u'user_id']) + '\"')
    elif text[1:7] == 'rechan':
        del_recourse(id)
        write_msg(item[u'user_id'],u'принята команда к удалению обращения!\nОбращение успешно удалено')
    elif text[1:7] == 'random':
        a = str(myrnd(text))
        write_msg(item[u'user_id'], u'' + a)
    elif text[1:7] == 'helper':
        write_msg(item[u'user_id'], u'привет!\nМеня зовут Сослан Сосланович\nЯ умею выполнять команды:\n\n/change <обращение> - меняет обращение к тебе\n/rechan - удаляет обращение к тебе\n\n/random - генерирует случайное число от 0 до 1\n/random int - генерирует целое неотрицательное число /random int <число> - генерирует целое неотрицательное не больше заданного\n/random int <число> <число> - генерирует целое число в заданном диапазоне\n/random abc <набор символов> - выбирает 1 символ из набора\n\n/secret add <название> <пароль> <текст> - добавить секрет\n/secret del <название> <пароль> - удаляет секрет\n/secret all - получить список своих секретов\n/secret get <название> <пароль> - получить секрет\n\n/pogoda now <город> - информация о текущей погоде\n/pogoda fut <город> - прогноз погоды\nСамое точное описание города выглядит так: Moscow,RU. При других способах задания города могут возникать ошибки\n\n/wolfik <запрос> - запрос к системе wolfram alpha\n\nПока что это все, что я умею!')
    elif text[1:7] == 'secret':
        ans = secret(text, item[u'user_id']);
        write_msg(item[u'user_id'], u'' + ans)
    elif text[1:7] == 'pogoda':
        answ = weather(text);
        write_msg(item[u'user_id'], u'' + answ)
    elif text[1:7] == 'wolfik':
        answer = wolframalpha_h(text)
        write_msg(item[u'user_id'], u'' + answer)
    else:
        write_msg(item[u'user_id'], u'команда не найдена')

def wolframalpha_h(text):
    pstrs = text.split(' ')

    if len(pstrs) == 1:
        return 'ошибка работы с wolfram alpha'

    res = wolfram.query(text[text.find(' ') + 1:len(text)])

    l = []
    for pod in res['pod']:
        for spod in pod['subpod']:
            try:
                if spod == 'img':
                    l.append(pod['subpod']['img']['@src'])
                else :
                    l.append(spod['img']['@src'])
            except:
                pass

    if len(l) == 0:
        return 'wolfram alpha не вернул никаких данных'
    
    s = ''
    for i in l:
        s = s + ' \n' + i
    return s;
        
def weather(text):
    
    c = 0;
    pr2 = 0;
    
    for i in text:
        c = c + 1;
        if i == ' ':
            pr2 = pr2 + 1;
            if pr2 == 2:
                pr2 = c
                break
   
    city_s, city_id = get_city_id(text[pr2:len(text)])

    if city_id == 0:
        return 'город не найден'

    if text[8:11] == 'now':
        return 'погода сейчас в ' + city_s + ':\n' + request_current_weather(city_id)
    elif text[8:11] == 'fut':
        return 'прогноз погоды в ' + city_s + ':' + request_forecast(city_id) #\n стоит в другом месте. Не магия
    else:
        return 'ошибка введенных данных'


        

    ddd = 0;        

def check_fio(id):
    if get_recourse(id):
        return 1
    return 0
         

while True:
    response = vk.method('messages.get', values)
    if response['items']:
        values['last_message_id'] = response['items'][0]['id']
    for item in response['items']:
        print("Пользователь с id " + str(item[u'user_id']) + " написал сообщение с текстом \"" +  item[u'body'] + "\"\n")
        text = item[u'body']
        if text:
            if text[0] == '/':
                hndl_cmd(text)
            else :
                write_msg(item[u'user_id'],u'я успешно прочел твое сообщение!')
        else :
            write_msg(item[u'user_id'],u'я могу понимать только текст!')




    